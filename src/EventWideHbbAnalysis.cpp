#include "EventWideHbbAnalysis.h"

#include "Defines.h"
#include "EventWideDefines.h"

EventWideHbbAnalysis::EventWideHbbAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC, const std::string& syst, const std::string& br_truth_pt)
  : m_isMC(isMC)
{
  df_root=rootdf;

  // Definitions of four-momenta
  df_defp4=std::make_shared<ROOT::RDF::RNode>((*df_root)
					      .Define("fatjet"+syst+"_p4", PtEtaPhiM, {"nfatjet"+syst,"fatjet"+syst+"_pt","fatjet"+syst+"_eta","fatjet"+syst+"_phi","fatjet"+syst+"_m"})
					      .Define("trkjet_p4", PtEtaPhiE, {"ntrkjet","trkjet_pt","trkjet_eta","trkjet_phi","trkjet_E"})
					      .Define("muon_p4", PtEtaPhiM, {"nmuon","muon_pt","muon_eta","muon_phi","muon_m"})
					      );

  if(isMC)
    df_deftr=std::make_shared<ROOT::RDF::RNode>((*df_defp4)
						.Define("truth_pt", [](float truth_pt)->float { return truth_pt; }, {br_truth_pt})
						);
  else
    df_deftr=df_defp4;

  df_prw=std::make_shared<ROOT::RDF::RNode>((*df_deftr)
					    .Define("weight_prw",[](float weight, float weight_pileup)->float { return weight*weight_pileup; }, {"weight_norm","weight_pileup"})
					    );

  if(isMC)
    df_trigger=std::make_shared<ROOT::RDF::RNode>((*df_prw)
						  .Filter(EventWide::trigger_mc,
							  {
							    "runNumber",
							      "trigger_HLT_j360_a10_lcw_sub_L1J100", // 2015
							      "trigger_HLT_j420_a10_lcw_L1J100", // 2016
							      "trigger_HLT_j390_a10t_lcw_jes_30smcINF_L1J100", "trigger_HLT_j440_a10t_lcw_jes_L1J100", // 2017
							      "trigger_HLT_j420_a10t_lcw_jes_35smcINF_L1J100", "trigger_HLT_j460_a10t_lcw_jes_L1J100" // 2018
							      }, "Trigger")
						  .Define("weight_trigger", [](int runNumber, float weight) -> float {
						      switch(runNumber)
							{
							case 284500: return 36.2e3*weight; //mc16a
							case 300000: return 41.0e3*weight; //mc16d
							case 310000: return 58.5e3*weight; //mc16e
							}
						      return 1.;
						    }, {"runNumber", "weight_prw"})
						  );
  else
    df_trigger=std::make_shared<ROOT::RDF::RNode>((*df_prw)
						  .Filter(EventWide::trigger_data,
							  {
							    "runNumber",
							      "trigger_HLT_j360_a10_lcw_sub_L1J100", // 2015
							      "trigger_HLT_j420_a10_lcw_L1J100", // 2016
							      "trigger_HLT_j390_a10t_lcw_jes_30smcINF_L1J100", "trigger_HLT_j440_a10t_lcw_jes_L1J100", // 2017
							      "trigger_HLT_j420_a10t_lcw_jes_35smcINF_L1J100", "trigger_HLT_j460_a10t_lcw_jes_L1J100" // 2018
							      }, "Trigger")
						  .Define("weight_trigger", []() -> float { return 1.; })
						  );

  df_triggerjet=std::make_shared<ROOT::RDF::RNode>((*df_trigger)
						   .Filter([](const std::vector<float>& fatjet_pt, const std::vector<float>& fatjet_m) -> bool { return trigger_jet_ptmass(fatjet_pt,fatjet_m,450,60); }, {"fatjet"+syst+"_pt","fatjet"+syst+"_m"} , "Trigger Jet")
						   );

  df_cleaning=std::make_shared<ROOT::RDF::RNode>((*df_triggerjet)
						 .Filter(filter_flag, {"eventClean_LooseBad"} , "Jet Cleaning")
						 );

  df_dijet=std::make_shared<ROOT::RDF::RNode>((*df_cleaning)
					      .Filter([](int32_t nfatjet, const std::vector<float>& fatjet_pt)->bool { return nfatjet>=2 && fatjet_pt[1]>200; }, {"nfatjet"+syst,"fatjet"+syst+"_pt"})
					      .Define("fatjet0"+syst+"_m", [](const std::vector<float>& fatjet_m)->float { return fatjet_m[0]; }, {"fatjet"+syst+"_m"})
					      .Define("fatjet1"+syst+"_m", [](const std::vector<float>& fatjet_m)->float { return fatjet_m[1]; }, {"fatjet"+syst+"_m"})
					      );

  df_trkJet5Idx =std::make_shared<ROOT::RDF::RNode>((*df_dijet     ).Define("fatjet"+syst+"_trkJets5Idx_GhostVR30Rmax4Rmin02TrackJet" , fatjet_trkjet5_idxs ,
  									    {"fatjet"+syst+"_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet","trkjet_pt"}));
  df_trkJet10Idx=std::make_shared<ROOT::RDF::RNode>((*df_trkJet5Idx).Define("fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet", fatjet_trkjet10_idxs,
  									    {"fatjet"+syst+"_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet","trkjet_pt"}));

    // Fat jet list of possible cadidates
  df_fatjet_ntj  =std::make_shared<ROOT::RDF::RNode>((*df_trkJet10Idx)
						     .Define("fatjet"+syst+"_ntj", count_subjet, {"fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet"})
						     );

  df_fatjet_nbin2=std::make_shared<ROOT::RDF::RNode>((*df_fatjet_ntj)
						     .Define("fatjet"+syst+"_nbin2", count_bjetsin2, {"fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_is_MV2c10_FixedCutBEff_77"})
						     );
  
  df_fatjet_vrolap=std::make_shared<ROOT::RDF::RNode>((*df_fatjet_nbin2)
						      .Define("trkjet_R", trackjet_VR, {"trkjet_pt"})
						      .Define("fatjet"+syst+"_VR_overlap", fatjet_VR_overlap, {"fatjet"+syst+"_trkJets5Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_p4","trkjet_R"})
						      );


  df_fatjet_boost=std::make_shared<ROOT::RDF::RNode>((*df_fatjet_vrolap)
						     .Define("fatjet"+syst+"_boost",fatjet_boost,{"fatjet"+syst+"_pt","fatjet"+syst+"_m"})
						     );

  df_fatjet_inlist=std::make_shared<ROOT::RDF::RNode>((*df_fatjet_boost)
						      .Define("fatjet"+syst+"_inlist",
							      [](const ROOT::VecOps::RVec<float>& fatjet_pt, const ROOT::VecOps::RVec<float>& fatjet_m, const ROOT::VecOps::RVec<float>& fatjet_boost, const ROOT::VecOps::RVec<uint32_t>& fatjet_ntj, const ROOT::VecOps::RVec<bool>& fatjet_VR_overlap)->ROOT::VecOps::RVec<bool>
							      { return (fatjet_ntj>=2)&&(fatjet_VR_overlap==false)&&(fatjet_pt>250)&&(fatjet_m>=60)&&(fatjet_boost<1); },
							      {"fatjet"+syst+"_pt","fatjet"+syst+"_m","fatjet"+syst+"_boost","fatjet"+syst+"_ntj","fatjet"+syst+"_VR_overlap"})
						      .Filter([](const ROOT::VecOps::RVec<bool>& fatjet_inlist)->bool { return fatjet_inlist[0] || fatjet_inlist[1]; },
							      {"fatjet"+syst+"_inlist"})
						      );

  // B-tagging
  df_category =std::make_shared<ROOT::RDF::RNode>((*df_fatjet_inlist)
						  .Define("SRL",[](const ROOT::VecOps::RVec<bool>& fatjet_inlist, const ROOT::VecOps::RVec<uint32_t>& fatjet_nbin2)->bool
							  { return fatjet_inlist[0] && fatjet_nbin2[0]==2; },
							  {"fatjet"+syst+"_inlist","fatjet"+syst+"_nbin2"})
						  .Define("SRS",[](const ROOT::VecOps::RVec<bool>& fatjet_inlist, const ROOT::VecOps::RVec<uint32_t>& fatjet_nbin2)->bool
							  { return fatjet_inlist[1] && fatjet_nbin2[1]==2 && !(fatjet_inlist[0] && fatjet_nbin2[0]==2); },
							  {"fatjet"+syst+"_inlist","fatjet"+syst+"_nbin2"})
						  .Define("VRL",[](const ROOT::VecOps::RVec<bool>& fatjet_inlist, const ROOT::VecOps::RVec<uint32_t>& fatjet_nbin2)->bool
							  { return fatjet_inlist[0] && fatjet_nbin2[0]==0 && !(fatjet_inlist[1] && (fatjet_nbin2[1]==1 || fatjet_nbin2[1]==2)); },
							  {"fatjet"+syst+"_inlist","fatjet"+syst+"_nbin2"})
						  .Define("VRS",[](const ROOT::VecOps::RVec<bool>& fatjet_inlist, const ROOT::VecOps::RVec<uint32_t>& fatjet_nbin2)->bool
							  { return fatjet_inlist[1] && fatjet_nbin2[1]==0 && !(fatjet_inlist[0] && (fatjet_nbin2[0]==1 || fatjet_nbin2[0]==2)); },
							  {"fatjet"+syst+"_inlist","fatjet"+syst+"_nbin2"})
						  .Filter(filter_or, {"SRL","SRS","VRL","VRS"})
						  );

  if(isMC)
    {
      df_truth  =std::make_shared<ROOT::RDF::RNode>((*df_category)
						    .Define("fatjet"+syst+"_nTruthB", [](const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int32_t>& trkjet_truth)->ROOT::VecOps::RVec<uint32_t>
							    { return count_truthjets(  5, fatjet_trkjet_idxs, trkjet_truth); },
							    {"fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_HadronConeExclTruthLabelID"})
						    .Define("fatjet"+syst+"_nTruthC", [](const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int32_t>& trkjet_truth)->ROOT::VecOps::RVec<uint32_t>
							    { return count_truthjets(  4, fatjet_trkjet_idxs, trkjet_truth); },
							    {"fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_HadronConeExclTruthLabelID"})
						    .Define("fatjet"+syst+"_nTruthL", [](const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int32_t>& trkjet_truth)->ROOT::VecOps::RVec<uint32_t>
							    { return count_truthjets(  0, fatjet_trkjet_idxs, trkjet_truth); },
							    {"fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_HadronConeExclTruthLabelID"})
						    .Define("fatjet"+syst+"_nTruthX", [](const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int32_t>& trkjet_truth)->ROOT::VecOps::RVec<uint32_t>
							    { return count_truthjets(-99, fatjet_trkjet_idxs, trkjet_truth); },
							    {"fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_HadronConeExclTruthLabelID"})
						    );
      
      df_srl_inc=Define_Hcand(std::make_shared<ROOT::RDF::RNode>((*df_truth)
								 .Filter("SRL")
								 ),0,syst);

      df_srs_inc=Define_Hcand(std::make_shared<ROOT::RDF::RNode>((*df_truth)
								 .Filter("SRS")
								 ),1,syst);

      df_vrl_inc=Define_Hcand(std::make_shared<ROOT::RDF::RNode>((*df_truth)
								 .Filter("VRL")
								 ),0,syst);

      df_vrs_inc=Define_Hcand(std::make_shared<ROOT::RDF::RNode>((*df_truth)
								 .Filter("VRS")
								 ),1,syst);
      
      //
      // b-tagging systematics loop
      uint32_t btagging_systematics_size=(syst.empty())?btagging_systematics.size():1;
      
      for(uint32_t btag_syst_idx=0;
	  btag_syst_idx<btagging_systematics_size;
	  btag_syst_idx++)
	{
	  std::string bsystname=btagging_systematics[btag_syst_idx];

	  df_srl_inc=std::make_shared<ROOT::RDF::RNode>((*df_srl_inc)
							.Define("w"+bsystname, [btag_syst_idx](float weight, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<std::vector<float>>& trkjet_sf)->float {
							    return weight*
							      trkjet_sf[fatjet_trkjet_idxs[0][0]][btag_syst_idx]*trkjet_sf[fatjet_trkjet_idxs[0][1]][btag_syst_idx];
							  },{"weight_trigger","fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_SF_MV2c10_FixedCutBEff_77"})
							);

	  df_srs_inc=std::make_shared<ROOT::RDF::RNode>((*df_srs_inc)
							.Define("w"+bsystname, [btag_syst_idx](float weight, const ROOT::VecOps::RVec<bool>& fatjet_inlist, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<std::vector<float>>& trkjet_sf)->float {
							    if(fatjet_inlist[0]) // Had to run b-tagging on the leading jet
							      return weight*
								trkjet_sf[fatjet_trkjet_idxs[0][0]][btag_syst_idx]*trkjet_sf[fatjet_trkjet_idxs[0][1]][btag_syst_idx]*
								trkjet_sf[fatjet_trkjet_idxs[1][0]][btag_syst_idx]*trkjet_sf[fatjet_trkjet_idxs[1][1]][btag_syst_idx];
							    else
							      return weight*
								trkjet_sf[fatjet_trkjet_idxs[1][0]][btag_syst_idx]*trkjet_sf[fatjet_trkjet_idxs[1][1]][btag_syst_idx];
							  },{"weight_trigger","fatjet"+syst+"_inlist","fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_SF_MV2c10_FixedCutBEff_77"})
							);

	  df_vrl_inc=std::make_shared<ROOT::RDF::RNode>((*df_vrl_inc)
							.Define("w"+bsystname, [btag_syst_idx](float weight, const ROOT::VecOps::RVec<bool>& fatjet_inlist, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<std::vector<float>>& trkjet_sf)->float {
							    if(fatjet_inlist[1]) // Had to run b-tagging on the leading and subleading jet
							      return weight*
								trkjet_sf[fatjet_trkjet_idxs[0][0]][btag_syst_idx]*trkjet_sf[fatjet_trkjet_idxs[0][1]][btag_syst_idx]*
								trkjet_sf[fatjet_trkjet_idxs[1][0]][btag_syst_idx]*trkjet_sf[fatjet_trkjet_idxs[1][1]][btag_syst_idx];
							    else
							      return weight*
								trkjet_sf[fatjet_trkjet_idxs[0][0]][btag_syst_idx]*trkjet_sf[fatjet_trkjet_idxs[0][1]][btag_syst_idx];
							  },{"weight_trigger","fatjet"+syst+"_inlist","fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_SF_MV2c10_FixedCutBEff_77"})
							);

	  df_vrs_inc=std::make_shared<ROOT::RDF::RNode>((*df_vrs_inc)
							.Define("w"+bsystname, [btag_syst_idx](float weight, const ROOT::VecOps::RVec<bool>& fatjet_inlist, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<std::vector<float>>& trkjet_sf)->float {
							    if(fatjet_inlist[0]) // Had to run b-tagging on the leading and subleading jet
							      return weight*
								trkjet_sf[fatjet_trkjet_idxs[0][0]][btag_syst_idx]*trkjet_sf[fatjet_trkjet_idxs[0][1]][btag_syst_idx]*
								trkjet_sf[fatjet_trkjet_idxs[1][0]][btag_syst_idx]*trkjet_sf[fatjet_trkjet_idxs[1][1]][btag_syst_idx];
							    else
							      return weight*
								trkjet_sf[fatjet_trkjet_idxs[1][0]][btag_syst_idx]*trkjet_sf[fatjet_trkjet_idxs[1][1]][btag_syst_idx];
							  },{"weight_trigger","fatjet"+syst+"_inlist","fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_SF_MV2c10_FixedCutBEff_77"})
							);
	}
    }
  else
    {
      df_srl_inc=std::make_shared<ROOT::RDF::RNode>(
						    Define_Hcand(std::make_shared<ROOT::RDF::RNode>((*df_category)
												    .Filter("SRL")
												    .Define("w", []()->float { return 1.; })
												    ),0,syst)
						    ->Filter([](float Hcand_m)->bool { return Hcand_m<70; },{"Hcand_m"})
						    );

      df_srs_inc=std::make_shared<ROOT::RDF::RNode>(
						    Define_Hcand(std::make_shared<ROOT::RDF::RNode>((*df_category)
												    .Filter("SRS")
												    .Define("w", []()->float { return 1.; })
												    ),1,syst)
						    ->Filter([](float Hcand_m)->bool { return Hcand_m<70; },{"Hcand_m"})
						    );

      df_vrl_inc=Define_Hcand(std::make_shared<ROOT::RDF::RNode>((*df_category)
								 .Filter("VRL")
								 .Define("w", []()->float { return 1.; })
								 ),0,syst);

      df_vrs_inc=Define_Hcand(std::make_shared<ROOT::RDF::RNode>((*df_category)
								 .Filter("VRS")
								 .Define("w", []()->float { return 1.; })
								 ),1,syst);
    }
}

std::shared_ptr<ROOT::RDF::RNode> EventWideHbbAnalysis::Define_Hcand(std::shared_ptr<ROOT::RDF::RNode> df, uint32_t Hcand_idx, const std::string& syst)
{
  if(m_isMC)
    {
      return std::make_shared<ROOT::RDF::RNode>((*df)
						.Define("Hcand_pt"        , [Hcand_idx](const std::vector<float>& fatjet_pt)->float { return fatjet_pt[Hcand_idx]; },{"fatjet"+syst+"_muonCorrected_pt"})
						.Define("Hcand_m"         , [Hcand_idx](const std::vector<float>& fatjet_m )->float { return fatjet_m [Hcand_idx]; },{"fatjet"+syst+"_muonCorrected_m" })
						.Define("Hcand_uncorr_pt" , [Hcand_idx](const std::vector<float>& fatjet_pt)->float { return fatjet_pt[Hcand_idx]; },{"fatjet"+syst+"_pt"})
						.Define("Hcand_uncorr_m"  , [Hcand_idx](const std::vector<float>& fatjet_m )->float { return fatjet_m [Hcand_idx]; },{"fatjet"+syst+"_m" })
						.Define("Hcand_nTruthB"   , [Hcand_idx](const ROOT::VecOps::RVec<uint32_t>& fatjet_nTruthB )->uint32_t { return fatjet_nTruthB[Hcand_idx]; },{"fatjet"+syst+"_nTruthB" })
						.Define("Hcand_nTruthC"   , [Hcand_idx](const ROOT::VecOps::RVec<uint32_t>& fatjet_nTruthC )->uint32_t { return fatjet_nTruthC[Hcand_idx]; },{"fatjet"+syst+"_nTruthC" })
						.Define("Hcand_nTruthL"   , [Hcand_idx](const ROOT::VecOps::RVec<uint32_t>& fatjet_nTruthL )->uint32_t { return fatjet_nTruthL[Hcand_idx]; },{"fatjet"+syst+"_nTruthL" })
						.Define("Hcand_nTruthX"   , [Hcand_idx](const ROOT::VecOps::RVec<uint32_t>& fatjet_nTruthX )->uint32_t { return fatjet_nTruthX[Hcand_idx]; },{"fatjet"+syst+"_nTruthX" })
						);
    }
  else
    {
      return std::make_shared<ROOT::RDF::RNode>((*df)
						.Define("Hcand_pt"        , [Hcand_idx](const std::vector<float>& fatjet_pt)->float { return fatjet_pt[Hcand_idx]; },{"fatjet"+syst+"_muonCorrected_pt"})
						.Define("Hcand_m"         , [Hcand_idx](const std::vector<float>& fatjet_m )->float { return fatjet_m [Hcand_idx]; },{"fatjet"+syst+"_muonCorrected_m" })
						.Define("Hcand_uncorr_pt" , [Hcand_idx](const std::vector<float>& fatjet_pt)->float { return fatjet_pt[Hcand_idx]; },{"fatjet"+syst+"_pt"})
						.Define("Hcand_uncorr_m"  , [Hcand_idx](const std::vector<float>& fatjet_m )->float { return fatjet_m [Hcand_idx]; },{"fatjet"+syst+"_m" })
						);
    }
}
