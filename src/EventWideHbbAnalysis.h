#ifndef EVENTWIDEHBBANALYSIS_H
#define EVENTWIDEHBBANALYSIS_H

#include <memory>

#include <ROOT/RDataFrame.hxx>

class EventWideHbbAnalysis
{
public:
  EventWideHbbAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC=false, const std::string& syst="", const std::string& br_truth_pt="Higgs_pt");

  // List of b-tagging systematics considered
  std::vector<std::string> btagging_systematics={"","FT_EFF_Eigen_B_0__1down","FT_EFF_Eigen_B_0__1up","FT_EFF_Eigen_B_1__1down","FT_EFF_Eigen_B_1__1up","FT_EFF_Eigen_B_2__1down","FT_EFF_Eigen_B_2__1up","FT_EFF_Eigen_B_3__1down","FT_EFF_Eigen_B_3__1up","FT_EFF_Eigen_B_4__1down","FT_EFF_Eigen_B_4__1up","FT_EFF_Eigen_C_0__1down","FT_EFF_Eigen_C_0__1up","FT_EFF_Eigen_C_1__1down","FT_EFF_Eigen_C_1__1up","FT_EFF_Eigen_C_2__1down","FT_EFF_Eigen_C_2__1up","FT_EFF_Eigen_C_3__1down","FT_EFF_Eigen_C_3__1up","FT_EFF_Eigen_C_4__1down","FT_EFF_Eigen_C_4__1up","FT_EFF_Eigen_Light_0__1down","FT_EFF_Eigen_Light_0__1up","FT_EFF_Eigen_Light_1__1down","FT_EFF_Eigen_Light_1__1up","FT_EFF_Eigen_Light_2__1down","FT_EFF_Eigen_Light_2__1up","FT_EFF_Eigen_Light_3__1down","FT_EFF_Eigen_Light_3__1up","FT_EFF_Eigen_Light_4__1down","FT_EFF_Eigen_Light_4__1up","FT_EFF_Eigen_Light_5__1down","FT_EFF_Eigen_Light_5__1up","FT_EFF_extrapolation__1down","FT_EFF_extrapolation__1up","FT_EFF_extrapolation_from_charm__1down","FT_EFF_extrapolation_from_charm__1up"};

  
  // Different nodes
  std::shared_ptr<ROOT::RDF::RNode> df_root;
  std::shared_ptr<ROOT::RDF::RNode> df_defp4;
  std::shared_ptr<ROOT::RDF::RNode> df_deftr;

  std::shared_ptr<ROOT::RDF::RNode> df_prw;
  std::shared_ptr<ROOT::RDF::RNode> df_trigger;
  std::shared_ptr<ROOT::RDF::RNode> df_triggerjet;
  std::shared_ptr<ROOT::RDF::RNode> df_cleaning;
  std::shared_ptr<ROOT::RDF::RNode> df_dijet;

  std::shared_ptr<ROOT::RDF::RNode> df_trkJet5Idx;
  std::shared_ptr<ROOT::RDF::RNode> df_trkJet10Idx;

  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_ntj;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_nbin2;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_vrolap;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_boost;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_inlist;

  std::shared_ptr<ROOT::RDF::RNode> df_truth;
  std::shared_ptr<ROOT::RDF::RNode> df_category;

  // Signal Region Leading
  std::shared_ptr<ROOT::RDF::RNode> df_srl_inc;

  // Signal Region Subleading
  std::shared_ptr<ROOT::RDF::RNode> df_srs_inc;

  // Validation Region Leading
  std::shared_ptr<ROOT::RDF::RNode> df_vrl_inc;

  // Validation Region Subleading
  std::shared_ptr<ROOT::RDF::RNode> df_vrs_inc;

private:
  std::shared_ptr<ROOT::RDF::RNode> Define_Hcand(std::shared_ptr<ROOT::RDF::RNode> df, uint32_t Hcand_idx, const std::string& syst);

  bool m_isMC = false;
};

#endif // EVENTWIDEHBBANALYSIS_H
