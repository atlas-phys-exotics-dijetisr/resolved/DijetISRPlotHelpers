#ifndef TRUTHHBBANALYSIS_H
#define TRUTHHBBANALYSIS_H

#include <memory>

#include <ROOT/RDataFrame.hxx>

class TruthHbbAnalysis
{
public:
  TruthHbbAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC=false);

  // Different nodes
  std::shared_ptr<ROOT::RDF::RNode> df_root;
  std::shared_ptr<ROOT::RDF::RNode> df_weight;

  std::shared_ptr<ROOT::RDF::RNode> df_triggerjet;
  std::shared_ptr<ROOT::RDF::RNode> df_dijet;

  std::shared_ptr<ROOT::RDF::RNode> df_Hcand;

  // Signal Region Leading
  std::shared_ptr<ROOT::RDF::RNode> df_srl_inc;

  // Signal Region Subleading
  std::shared_ptr<ROOT::RDF::RNode> df_srs_inc;

private:
  bool m_isMC = false;
};

#endif // TRUTHHBBANALYSIS_H
