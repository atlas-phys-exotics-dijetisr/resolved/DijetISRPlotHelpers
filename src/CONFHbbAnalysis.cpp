#include "CONFHbbAnalysis.h"

#include "Defines.h"

CONFHbbAnalysis::CONFHbbAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC, const std::string& syst)
{
  df_root=rootdf;

  // Definitions of four-momenta
  df_defp4=std::make_shared<ROOT::RDF::RNode>((*df_root)
					      .Define("fatjet"+syst+"_p4", PtEtaPhiM, {"nfatjet"+syst,"fatjet"+syst+"_pt","fatjet"+syst+"_eta","fatjet"+syst+"_phi","fatjet"+syst+"_m"})
					      .Define("trkjet_p4", PtEtaPhiE, {"ntrkjet","trkjet_pt","trkjet_eta","trkjet_phi","trkjet_E"})
					      .Define("muon_p4", PtEtaPhiM, {"nmuon","muon_pt","muon_eta","muon_phi","muon_m"}));

  df_prw=std::make_shared<ROOT::RDF::RNode>((*df_defp4).Define("weight_prw","weight_norm*weight_pileup"));
  
  if(isMC)
    df_trigger=std::make_shared<ROOT::RDF::RNode>((*df_prw)
						  .Filter("trigger_HLT_j360_a10_lcw_sub_L1J100 || trigger_HLT_j420_a10_lcw_L1J100 || trigger_HLT_j460_a10t_lcw_jes_L1J100", "Trigger")
						  .Define("weight_trigger", [](int runNumber, double weight) -> double {
						      switch(runNumber)
							{
							case 284500: return 36.2e3*weight; //mc16a
							case 300000: return 44.3e3*weight; //mc16d
							  //case 310000: return 58.5e3*weight; //mc16e
							}
						      return 1.;
						    }, {"runNumber", "weight_prw"})
						  );
  else
    df_trigger=std::make_shared<ROOT::RDF::RNode>((*df_prw)
						  .Filter("trigger_HLT_j360_a10_lcw_sub_L1J100 || trigger_HLT_j420_a10_lcw_L1J100 || trigger_HLT_j460_a10t_lcw_jes_L1J100", "Trigger")
						  .Define("weight_trigger", []() -> double { return 1.;})
						  );

  df_cleaning=std::make_shared<ROOT::RDF::RNode>((*df_trigger)
						 .Filter("eventClean_LooseBad", "Jet Cleaning")
						 );

  df_dijet=std::make_shared<ROOT::RDF::RNode>((*df_cleaning).Filter("nfatjet"+syst+">=2 && fatjet"+syst+"_pt[1]>250", "Dijet"));

  df_trkJet5Idx =std::make_shared<ROOT::RDF::RNode>((*df_dijet     ).Define("fatjet"+syst+"_trkJets5Idx_GhostVR30Rmax4Rmin02TrackJet" , fatjet_trkjet5_idxs ,
  									    {"fatjet"+syst+"_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet","trkjet_pt"}));
  df_trkJet10Idx=std::make_shared<ROOT::RDF::RNode>((*df_trkJet5Idx).Define("fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet", fatjet_trkjet10_idxs,
									    {"fatjet"+syst+"_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet","trkjet_pt"}));

  df_fatjet_idx=std::make_shared<ROOT::RDF::RNode>((*df_trkJet10Idx).Define("fatjet"+syst+"_idx", indexlist, {"nfatjet"+syst}));

  // LargeR list
  df_fatjet_ntj   =std::make_shared<ROOT::RDF::RNode>((*df_fatjet_idx).Define("fatjet"+syst+"_ntj"  , count_subjet     , {"fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet"}));
  df_fatjet_nbin2 =std::make_shared<ROOT::RDF::RNode>((*df_fatjet_ntj).Define("fatjet"+syst+"_nbin2", count_bjetsin2   , {"fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_is_MV2c10_FixedCutBEff_77"}));  
  df_fatjet_vrolap=std::make_shared<ROOT::RDF::RNode>((*df_fatjet_nbin2)
						      .Define("trkjet_R"                            , trackjet_VR      , {"trkjet_pt"})
						      .Define("fatjet"+syst+"_VR_overlap"           , fatjet_VR_overlap, {"fatjet"+syst+"_trkJets5Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_p4","trkjet_R"})
						      );
  df_fatjet_boost=std::make_shared<ROOT::RDF::RNode>((*df_fatjet_vrolap)
						     .Define("fatjet"+syst+"_boost",fatjet_boost,{"fatjet"+syst+"_pt","fatjet"+syst+"_m"})
						     );

  df_fatjetlist=std::make_shared<ROOT::RDF::RNode>((*df_fatjet_boost)
						   .Define("fatjetlist_idx", "fatjet"+syst+"_idx[(fatjet"+syst+"_ntj>=2)&&(fatjet"+syst+"_VR_overlap==false)&&(fatjet"+syst+"_boost<1)]")
						   );

  // Signal candidate definition
  df_category =std::make_shared<ROOT::RDF::RNode>((*df_fatjetlist)
						  .Filter("fatjetlist_idx.size()>0", "Region")
						  .Define("Hcand_idx", "fatjetlist_idx[0]"));

  df_Hcand=std::make_shared<ROOT::RDF::RNode>((*df_category)
					      .Define("Hcand_pt","fatjet"+syst+"_pt[Hcand_idx]")
					      .Define("Hcand_m" ,"fatjet"+syst+"_m [Hcand_idx]")
					      .Filter("Hcand_pt>480","Hcand pT")
					      .Filter("Hcand_m>60", "Hcand M")
					      );

  if(isMC)
    df_sf=std::make_shared<ROOT::RDF::RNode>((*df_Hcand)
					     .Define("w", [](double weight, uint32_t Hcand_idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<std::vector<float>>& trkjet_sf)->double {
						 return weight*trkjet_sf[fatjet_trkjet_idxs[Hcand_idx][0]][0]*trkjet_sf[fatjet_trkjet_idxs[Hcand_idx][1]][0];
					       },{"weight_trigger","Hcand_idx","fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet","trkjet_SF_MV2c10_FixedCutBEff_77"})
					     );
  else
    df_sf=std::make_shared<ROOT::RDF::RNode>((*df_Hcand)
					     .Define("w", []()->double { return 1.; })
					     );

  // Region categorization
  df_sr=std::make_shared<ROOT::RDF::RNode>((*df_sf).Filter("fatjet"+syst+"_nbin2[Hcand_idx]==2","Signal Region"));
  df_vr=std::make_shared<ROOT::RDF::RNode>((*df_sf).Filter("fatjet"+syst+"_nbin2[Hcand_idx]==0","Validation Region"));
}
