#include <iostream>
#include <fstream>
#include <chrono>
#include <algorithm>

#include <TFile.h>
#include <TH1D.h>
#include <TLorentzVector.h>
#include <ROOT/RDataFrame.hxx>

#include "Defines.h"
#include "SampleFactory.h"
#include "Sample.h"
#include "EventWideHbbAnalysis.h"

int main(int argc, char* argv[])
{
  // Parse arguments
  if(argc<3)
    {
      std::cerr << "usage: " << argv[0] << " nthreads outDir [sample [systematic]]" << std::endl;
      return 1;
    }
  uint32_t nthreads=std::atoi(argv[1]);
  std::string outDir=argv[2];
  std::vector<std::string> saves={"Pythia8_dijet.mc16a","Sherpa_Zqq.mc16a","Sherpa_Wqq.mc16a","Pythia8_ttbar.mc16a","data1516"};
  std::vector<std::string> systematics;

  if(argc>3)
    saves={argv[3]};

  if(argc>4)
    {
      std::stringstream sstr(argv[4]);
      std::string systematic;
      while(std::getline(sstr, systematic, ','))
	{
	  if(systematic=="nominal")
	    systematics.push_back("");
	  else
	    systematics.push_back(systematic);
	}
    }

  ROOT::EnableImplicitMT(nthreads);  

  // Process stuff
  SampleFactory sf("../samples.yml");

  // Histogram list
  std::vector<ROOT::RDF::TH1DModel> hists;
  hists.push_back({"Hcand_m"       ,"", 48,0, 240});
  hists.push_back({"Hcand_pt"      ,"",200,0,2000});
  hists.push_back({"Hcand_uncorr_m"  ,"", 48,0, 240});
  hists.push_back({"Hcand_uncorr_pt" ,"",200,0,2000});

  std::map<std::string, std::shared_ptr<ROOT::RDF::RNode> EventWideHbbAnalysis::*> selections;
  selections["srl"]=&EventWideHbbAnalysis::df_srl_inc;
  selections["srs"]=&EventWideHbbAnalysis::df_srs_inc;
  selections["vrl"]=&EventWideHbbAnalysis::df_vrl_inc;
  selections["vrs"]=&EventWideHbbAnalysis::df_vrs_inc;

  std::vector<std::string> cachelist={"w"}; // Cache branches for each selection
  std::transform(hists.begin(),hists.end(),std::back_inserter(cachelist),
		 [](const ROOT::RDF::TH1DModel& histDef) { return histDef.fName.Data(); });
  
  // Analysis
  ROOT::RDF::RResultPtr<TH1D> h;

  for(const std::string& save : saves)
    {
      std::cout << "Processing " << save << std::endl;
      if(!sf.contains(save))
	{
	  std::cerr << "WARNING: Unable to find \"" << save << "\"! Skip..." << std::endl;
	  continue;
	}

      for(const std::string& fatjetSyst : systematics)
	{
	  std::string outFile=outDir+"/"+save;
	  
	  if(fatjetSyst.empty())
	    {
	      std::cout << "\tnominal" << std::endl;
	      outFile+=".root";
	    }
	  else
	    {
	      std::cout << "\tsystematic " << fatjetSyst << std::endl;
	      outFile+="_"+fatjetSyst+".root";
	    }

	  EventWideHbbAnalysis a(sf[save]->dataframe(),sf[save]->isMC(), fatjetSyst);

	  TFile *fh_out=TFile::Open(outFile.c_str(),"RECREATE");	  
	  for(const std::pair<std::string, std::shared_ptr<ROOT::RDF::RNode> EventWideHbbAnalysis::*> selection : selections)
	    {
	      std::string mypath;
	      if(selections.empty())
	  	mypath=selection.first;
	      else
	  	mypath=selection.first+"/"+fatjetSyst;
	      fh_out->mkdir(mypath.c_str());
	      fh_out->cd   (mypath.c_str());

	      ROOT::RDF::RNode cache=(a.*(selection.second))->Cache(cachelist);
	      for(const ROOT::RDF::TH1DModel& hdef : hists)
	  	{
	  	  h=cache.Histo1D(hdef,hdef.fName.Data(),"w");
	  	  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
	  	  h->Write(hdef.fName);
	  	  std::chrono::steady_clock::time_point end   = std::chrono::steady_clock::now();
	  	  std::cout << "\t\t" << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " ms" << std::endl;
	  	}
	    }
	  fh_out->Close();	  
	}
    }
}
