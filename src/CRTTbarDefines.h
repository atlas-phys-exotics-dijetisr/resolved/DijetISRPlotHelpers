#ifndef CRTTBARDEFINES_H_
#define CRTTBARDEFINES_H_

#include <stdint.h>

#include <vector>

#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>

namespace CRTTbar
{
  //! Tag jet idx (matched to the leading muon)
  /**
   * Tag fatjet that is the closest fatjet to tag muon given pT and dR requirements
   *
   * \return index of a tag fatjet, 666 if not found
   */
  uint32_t tag_fatjet_idx(const ROOT::VecOps::RVec<TLorentzVector>& muon_p4, const ROOT::VecOps::RVec<TLorentzVector>& fatjet_p4);

  //! Probe fatjet idx (one of the leading jets that is not tag jet)
  /**
   * \return probe fatjet idx
   */
  uint32_t probe_fatjet_idx(uint32_t tag_fatjet_idx);

  // T/F if the tag and probe jets are back to back
  bool filter_probe_tag_deltaPhi(uint32_t tag_fatjet_idx, uint32_t probe_fatjet_idx, const ROOT::VecOps::RVec<TLorentzVector>& fatjet_p4);
  // Number of b-tags in the leading jet
  uint32_t tagjet_nbtag(uint32_t tag_fatjet_idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const ROOT::VecOps::RVec<bool>& trkjet_btag);

  //! Number of b-tags in the two leading jets
  /**
   * If jet has less than two track jets, only those are considered.
   *
   * \param probe_fatjet_idx Index of the probe jet
   * \param fatjet_trkjet_idxs fat jet to track jet association
   * \param trkjet_btag b-tagging decision on all track jets in the event
   *
   * \return Number of b-tags in the three leading track jets
   */
  uint32_t probejet_nbtag(uint32_t probe_fatjet_idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const ROOT::VecOps::RVec<bool>& trkjet_btag);
  
  //! product of btag SFs
  double btagSF(uint32_t tag_fatjet_idx, uint32_t probe_fatjet_idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<std::vector<float>>& trkjet_SF, uint32_t trkjet_SF_idx=0);
};

#endif // CRTTBARDEFINES_H_
