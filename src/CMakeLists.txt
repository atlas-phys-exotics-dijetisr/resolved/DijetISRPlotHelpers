ADD_LIBRARY(HbbPlotHelpers SHARED)
TARGET_SOURCES(HbbPlotHelpers PRIVATE
  Defines.cpp
  Sample.cpp
  SampleFilelist.cpp
  SampleMulti.cpp
  SampleFactory.cpp
  EventWideDefines.cpp
  EventWideHbbAnalysis.cpp
  ListBasedHbbAnalysis.cpp
  TruthHbbAnalysis.cpp
  CONFHbbAnalysis.cpp
  CRTTbarDefines.cpp
  CRTTbarAnalysis.cpp
  JetTriggerAnalysis.cpp)
TARGET_INCLUDE_DIRECTORIES(HbbPlotHelpers PRIVATE ${ROOT_INCLUDE_DIRS} ${YAML_CPP_INCLUDE_DIR})
TARGET_LINK_LIBRARIES(HbbPlotHelpers ${ROOT_LIBRARIES} ${YAML_CPP_LIBRARIES})


ADD_EXECUTABLE(testCONF)
TARGET_SOURCES(testCONF PRIVATE testCONF.cpp)
TARGET_LINK_LIBRARIES(testCONF HbbPlotHelpers ${ROOT_LIBRARIES})

ADD_EXECUTABLE(treeJetTrigger)
TARGET_SOURCES(treeJetTrigger PRIVATE treeJetTrigger.cpp)
TARGET_LINK_LIBRARIES(treeJetTrigger HbbPlotHelpers ${ROOT_LIBRARIES})

ADD_EXECUTABLE(testListBased)
TARGET_SOURCES(testListBased PRIVATE testListBased.cpp)
TARGET_LINK_LIBRARIES(testListBased HbbPlotHelpers ${ROOT_LIBRARIES})

ADD_EXECUTABLE(testEventWide)
TARGET_SOURCES(testEventWide PRIVATE testEventWide.cpp)
TARGET_LINK_LIBRARIES(testEventWide HbbPlotHelpers ${ROOT_LIBRARIES})

ADD_EXECUTABLE(cutflowEventWide)
TARGET_SOURCES(cutflowEventWide PRIVATE cutflowEventWide.cpp)
TARGET_LINK_LIBRARIES(cutflowEventWide HbbPlotHelpers ${ROOT_LIBRARIES})

ADD_EXECUTABLE(treeTruth)
TARGET_SOURCES(treeTruth PRIVATE treeTruth.cpp)
TARGET_LINK_LIBRARIES(treeTruth HbbPlotHelpers ${ROOT_LIBRARIES})

ADD_EXECUTABLE(treeCONF)
TARGET_SOURCES(treeCONF PRIVATE treeCONF.cpp)
TARGET_LINK_LIBRARIES(treeCONF HbbPlotHelpers ${ROOT_LIBRARIES})

ADD_EXECUTABLE(treeListBased)
TARGET_SOURCES(treeListBased PRIVATE treeListBased.cpp)
TARGET_LINK_LIBRARIES(treeListBased HbbPlotHelpers ${ROOT_LIBRARIES})

ADD_EXECUTABLE(treeEventWide)
TARGET_SOURCES(treeEventWide PRIVATE treeEventWide.cpp)
TARGET_LINK_LIBRARIES(treeEventWide HbbPlotHelpers ${ROOT_LIBRARIES})

ADD_EXECUTABLE(treeCRTTbar)
TARGET_SOURCES(treeCRTTbar PRIVATE treeCRTTbar.cpp)
TARGET_LINK_LIBRARIES(treeCRTTbar HbbPlotHelpers ${ROOT_LIBRARIES})

ADD_EXECUTABLE(testTime)
TARGET_SOURCES(testTime PRIVATE testTime.cpp)
TARGET_LINK_LIBRARIES(testTime HbbPlotHelpers ${ROOT_LIBRARIES})

ADD_EXECUTABLE(cutflowchallengeEventWide)
TARGET_SOURCES(cutflowchallengeEventWide PRIVATE cutflowchallengeEventWide.cpp)
TARGET_LINK_LIBRARIES(cutflowchallengeEventWide HbbPlotHelpers ${ROOT_LIBRARIES})

ADD_EXECUTABLE(cutflowchallengeListBased)
TARGET_SOURCES(cutflowchallengeListBased PRIVATE cutflowchallengeListBased.cpp)
TARGET_LINK_LIBRARIES(cutflowchallengeListBased HbbPlotHelpers ${ROOT_LIBRARIES})
