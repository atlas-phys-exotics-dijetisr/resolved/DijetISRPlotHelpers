#include "SampleFactory.h"

#include "SampleFilelist.h"
#include "SampleMulti.h"

#include <iostream>
#include <regex>

#include <yaml-cpp/yaml.h>

SampleFactory::SampleFactory(const std::string& samplesFile)
{
  YAML::Node config = YAML::LoadFile(samplesFile);

  for(const YAML::detail::iterator_value& sampleDef : config)
    {
      std::string sampleName=sampleDef.first.Scalar();
      YAML::Node sampleNode=sampleDef.second;

      // Common properties
      bool isMC    =sampleNode["mc"].Scalar()=="true";
      bool weighted=sampleNode["weighted"].Scalar()!="false";
      double scale =(sampleNode["scale"].IsDefined())?std::stod(sampleNode["scale"].Scalar()):1.;
      uint32_t weightIdx=(sampleNode["weightIdx"].IsDefined())?std::stoi(sampleNode["weightIdx"].Scalar()):0;
      
      //
      // Filelist definition
      if(sampleNode["filelists"].IsDefined())
	{
	  if(sampleNode["campaigns"].IsDefined())
	    { // Multiple campaign samples
	      std::shared_ptr<SampleMulti> sample=std::make_shared<SampleMulti>();
	      sample->setWeightIdx(weightIdx);

	      for(const YAML::Node& campaignNode : sampleNode["campaigns"])
		{
		  std::shared_ptr<SampleFilelist> campsample=std::make_shared<SampleFilelist>(isMC);
		  campsample->setWeighted(weighted);
		  campsample->setScale(scale);
		  campsample->setWeightIdx(weightIdx);

		  // Loop over a dataset
		  for(const YAML::Node& filelistNode : sampleNode["filelists"])
		    {
		      std::string filelist=std::regex_replace(filelistNode.Scalar(), std::regex("CAMP"), campaignNode.Scalar());
		      campsample->addFilelist(filelist);
		    }
		  sample->addSample(campsample);

		  m_samples[sampleName+"."+campaignNode.Scalar()]=campsample;
		}
	      m_samples[sampleName]=sample;
	    }
	  else
	    { // Single sample
	      std::shared_ptr<SampleFilelist> sample=std::make_shared<SampleFilelist>(isMC);
	      sample->setWeighted(weighted);
	      sample->setScale(scale);
	      sample->setWeightIdx(weightIdx);

	      // Loop over a dataset
	      for(const YAML::Node& filelistNode : sampleNode["filelists"])
		sample->addFilelist(filelistNode.Scalar());

	      m_samples[sampleName]=sample;
	    }
	}

      //
      // Files definition
      else if(sampleNode["files"].IsDefined())
	{
	  std::shared_ptr<SampleFilelist> sample=std::make_shared<SampleFilelist>(isMC);
	  sample->setWeighted(weighted);
	  sample->setScale(scale);
	  sample->setWeightIdx(weightIdx);

	  // Loop over a dataset
	  for(const YAML::Node& fileNode : sampleNode["files"])
	    {
	      sample->addFile(fileNode.Scalar());
	    }

	  m_samples[sampleName]=sample;
	}
      
      //
      // Sample definition
      else if(sampleNode["samples"].IsDefined())
	{
	  std::shared_ptr<SampleMulti> sample=std::make_shared<SampleMulti>();
	  sample->setScale(scale);

	  for(const YAML::Node& sampleNode : sampleNode["samples"])
	    {
	      if(m_samples.find(sampleNode.Scalar())==m_samples.end())
		{
		  std::cerr << "ERROR: Cannot find subsample '" << sampleNode.Scalar() << "' for " << sampleName << std::endl;
		  throw std::string("Cannot find subsample '" + sampleNode.Scalar() + "' for " + sampleName);
		}

	      sample->addSample(m_samples[sampleNode.Scalar()]);
	      // Inherit some settings
	      sample->setWeightIdx(m_samples[sampleNode.Scalar()]->weightIdx());
	    }

	  // Overwrite values
	  if(sampleNode["weightIdx"].IsDefined())
	    sample->setWeightIdx(weightIdx);

	  m_samples[sampleName]=sample;
	}
    }
}

bool SampleFactory::contains(const std::string& sampleName) const
{
  return m_samples.find(sampleName)!=m_samples.end();
}

std::shared_ptr<Sample> SampleFactory::operator[] (const std::string& sampleName)
{
  return m_samples[sampleName];
}
