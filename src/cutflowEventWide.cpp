#include <iostream>
#include <fstream>
#include <chrono>
#include <algorithm>
#include <iomanip>

#include <TFile.h>
#include <TH1D.h>
#include <TLorentzVector.h>
#include <ROOT/RDataFrame.hxx>

#include "SampleFactory.h"
#include "EventWideHbbAnalysis.h"

struct CutflowStep
{
  std::string name;
  std::shared_ptr<ROOT::RDF::RNode> EventWideHbbAnalysis::* node;
  std::string weight_column;
};

int main(int argc, char* argv[])
{
  ROOT::EnableImplicitMT(16);

  // Parse arguments
  if(argc<3)
    {
      std::cerr << "usage: " << argv[0] << " outdir [sample]" << std::endl;
      return 1;
    }
  std::string outDir=argv[1];
  std::vector<std::string> saves={"Higgs_ggf","Higgs_VBF","Higgs_VH","Higgs_ttH","data","Pythia8_dijet","Higgs","Powheg_ttbar","Sherpa_Zqq","Sherpa_Wqq"};  
  if(argc==3)
    saves={argv[2]};

  //
  // Define steps in the cutflow
  std::vector<CutflowStep> steps;
  steps.push_back({"Trigger"     ,&EventWideHbbAnalysis::df_trigger      , "weight_trigger"});
  steps.push_back({"TriggerJet"  ,&EventWideHbbAnalysis::df_triggerjet   , "weight_trigger"});
  steps.push_back({"JetCleaning" ,&EventWideHbbAnalysis::df_cleaning     , "weight_trigger"});
  steps.push_back({"Dijet"       ,&EventWideHbbAnalysis::df_dijet        , "weight_trigger"});
  steps.push_back({"JetList"     ,&EventWideHbbAnalysis::df_fatjet_inlist, "weight_trigger"});
  steps.push_back({"SRL"         ,&EventWideHbbAnalysis::df_srl_inc      , "w"             });
  steps.push_back({"SRS"         ,&EventWideHbbAnalysis::df_srs_inc      , "w"             });
  steps.push_back({"VRL"         ,&EventWideHbbAnalysis::df_vrl_inc      , "w"             });
  steps.push_back({"VRS"         ,&EventWideHbbAnalysis::df_vrs_inc      , "w"             });

  //
  // Loop over samples
  SampleFactory sf("../samples.yml");

  for(const std::string& save : saves)
    {
      std::string outFile=outDir+"/cutflow_"+save+".root";
      TFile *fh_out=TFile::Open(outFile.c_str(),"RECREATE");

      std::cout << "Processing " << save << std::endl;
      TH1F *h_cutflow=new TH1F(("cutflow_"+save).c_str(),"",steps.size(),-0.5,steps.size()-0.5);      
      if(!sf.contains(save))
	{
	  std::cerr << "WARNING: Unable to find \"" << save << "\"! Skip..." << std::endl;
	  continue;
	}
      EventWideHbbAnalysis a(sf[save]->dataframe(),sf[save]->isMC());	

      uint32_t binidx=1;
      for(const CutflowStep& step : steps)
	{
	  std::cout << " " << step.name << std::endl;
	  h_cutflow->GetXaxis()->SetBinLabel(binidx, step.name.c_str());
	  double nEvents=(a.*(step.node))->Sum(step.weight_column).GetValue();
	  h_cutflow->SetBinContent(binidx, nEvents);
	  ++binidx;
	}
      fh_out->cd();
      h_cutflow->Write();
      fh_out->Close();
    }
  
  return 0;
}
