#include "EventWideDefines.h"

namespace EventWide
{
  bool trigger_data(int32_t runNumber,
		    int8_t HLT_j360_a10_lcw_sub_L1J100, // 2015
		    int8_t HLT_j420_a10_lcw_L1J100, //2016
		    int8_t HLT_j390_a10t_lcw_jes_30smcINF_L1J100, int8_t HLT_j440_a10t_lcw_jes_L1J100, //2017
		    int8_t HLT_j420_a10t_lcw_jes_35smcINF_L1J100, int8_t HLT_j460_a10t_lcw_jes_L1J100 //2018
		    )
  {
    if(276262<=runNumber && runNumber<=284484)
      return (HLT_j360_a10_lcw_sub_L1J100==1);
    if(297730<=runNumber && runNumber<=311481)
      return (HLT_j420_a10_lcw_L1J100==1);
    if(325713<=runNumber && runNumber<=340453)
      return (HLT_j390_a10t_lcw_jes_30smcINF_L1J100==1) || (HLT_j390_a10t_lcw_jes_30smcINF_L1J100>=0 && HLT_j440_a10t_lcw_jes_L1J100==1);
    if(348885<=runNumber && runNumber<=364292)
      return (HLT_j420_a10t_lcw_jes_35smcINF_L1J100==1) || (HLT_j420_a10t_lcw_jes_35smcINF_L1J100>=0 && HLT_j460_a10t_lcw_jes_L1J100==1);
    return false;
  }

  bool trigger_mc(int32_t runNumber,
		  int8_t HLT_j360_a10_lcw_sub_L1J100, // 2015
		  int8_t HLT_j420_a10_lcw_L1J100, //2016
		  int8_t HLT_j390_a10t_lcw_jes_30smcINF_L1J100, int8_t HLT_j440_a10t_lcw_jes_L1J100, //2017
		  int8_t HLT_j420_a10t_lcw_jes_35smcINF_L1J100, int8_t HLT_j460_a10t_lcw_jes_L1J100 //2018
		  )
  {
    switch(runNumber)
      {
      case 284500: //mc16a
	return (HLT_j360_a10_lcw_sub_L1J100==1 || HLT_j420_a10_lcw_L1J100==1);
      case 300000: //mc16d
	return (HLT_j390_a10t_lcw_jes_30smcINF_L1J100==1) || (HLT_j390_a10t_lcw_jes_30smcINF_L1J100>=0 && HLT_j440_a10t_lcw_jes_L1J100==1);	
      case 310000: //mc16e
	return (HLT_j420_a10t_lcw_jes_35smcINF_L1J100==1) || (HLT_j420_a10t_lcw_jes_35smcINF_L1J100>=0 && HLT_j460_a10t_lcw_jes_L1J100==1);
      default:
	return false;
      }
  }
};
