#include "CRTTbarDefines.h"

namespace CRTTbar
{
  uint32_t tag_fatjet_idx(const ROOT::VecOps::RVec<TLorentzVector>& muon_p4, const ROOT::VecOps::RVec<TLorentzVector>& fatjet_p4)
  {
    const TLorentzVector& crttbar_muon = muon_p4[0];
    float min_dR = 0.04 + 10.0/crttbar_muon.Pt();

    // Loop over fatjets, find closest one with leading btag and pT > 250GeV
    for (uint32_t i = 0; i < std::min<uint32_t>(2, fatjet_p4.size()); ++i)
      {
	// Same dR requirements (just in case)
	float dR = crttbar_muon.DeltaR(fatjet_p4[i]);
	// to get closest fatjet
	if (dR > min_dR && dR < 1.5)
	  return i;
      }
    return 666;
  }

  uint32_t probe_fatjet_idx(uint32_t tag_fatjet_idx)
  {
    return (tag_fatjet_idx+1)%2;
  }

  // returns whether the opposite hemisphere requirement of probe and tag jet is satisfied
  bool filter_probe_tag_deltaPhi(uint32_t tag_fatjet_idx, uint32_t probe_fatjet_idx, const ROOT::VecOps::RVec<TLorentzVector>& fatjet_p4)
  {
    const TLorentzVector& tag_jet   = fatjet_p4[tag_fatjet_idx  ];
    const TLorentzVector& probe_jet = fatjet_p4[probe_fatjet_idx];

    float dPhi = fabs(tag_jet.DeltaPhi(probe_jet));
    return dPhi > 2*M_PI/3;
  }

  // returns number of b-tags in tag jet
  uint32_t tagjet_nbtag(uint32_t tag_fatjet_idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const ROOT::VecOps::RVec<bool>& trkjet_btag)
  {
    // Loop over btags, up to 1 of them.
    uint32_t n_bjets = 0;
    const std::vector<uint32_t>& fatjet_trkjet_idx = fatjet_trkjet_idxs[tag_fatjet_idx];
    for (uint32_t j = 0; j < std::min<int>(1, fatjet_trkjet_idx.size()); ++j)
      n_bjets += trkjet_btag[fatjet_trkjet_idx[j]];

    return n_bjets;
  }

  uint32_t probejet_nbtag(uint32_t probe_fatjet_idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const ROOT::VecOps::RVec<bool>& trkjet_btag)
  {
    // Loop over btags, up to 2 of them. (TODO: maybe 3 later)
    uint32_t n_bjets = 0;
    const std::vector<uint32_t>& fatjet_trkjet_idx = fatjet_trkjet_idxs[probe_fatjet_idx];
    for (uint32_t j = 0; j < std::min<int>(2, fatjet_trkjet_idx.size()); ++j)
      n_bjets += trkjet_btag[fatjet_trkjet_idx[j]];

    return n_bjets;
  }

  // returns SF from b-tags in tag jet
  double btagSF(uint32_t tag_fatjet_idx, uint32_t probe_fatjet_idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<std::vector<float>>& trkjet_SF, uint32_t trkjet_SF_idx)
  {
    double SF = 1.0;
    // Get SF from leading btagged vrJet in tagJet
    const std::vector<uint32_t>& tag_fatjet_trkjet_idx = fatjet_trkjet_idxs[tag_fatjet_idx];
    for (uint32_t j = 0; j < std::min<int>(1, tag_fatjet_trkjet_idx.size()); ++j)
      SF *= trkjet_SF[tag_fatjet_trkjet_idx[j]][trkjet_SF_idx];
    // Get SF(s) from trkjets in probe fatjet
    const std::vector<uint32_t>& probe_fatjet_trkjet_idx = fatjet_trkjet_idxs[probe_fatjet_idx];
    for (uint32_t j = 0; j < std::min<int>(2, probe_fatjet_trkjet_idx.size()); ++j)
      SF *= trkjet_SF[probe_fatjet_trkjet_idx[j]][trkjet_SF_idx];

    return SF;
  }
}
