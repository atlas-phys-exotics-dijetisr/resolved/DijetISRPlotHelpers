#include "SampleFilelist.h"

#include <TFile.h>
#include <iostream>

SampleFilelist::SampleFilelist(const std::vector<std::string>& filelist, bool isMC)
  : Sample(),m_isMC(isMC),m_filelist(filelist)
{ }

SampleFilelist::SampleFilelist(bool isMC)
  : Sample(),m_isMC(isMC)
{ }

void SampleFilelist::addFile(const std::string& path)
{
  m_filelist.push_back(path);
}

void SampleFilelist::addFilelist(const std::string& path)
{
  std::ifstream fh_fl(path);

  std::string line;
  while(std::getline(fh_fl,line))
    {
      if(line=="") continue;
      addFile(line);
    }
}

bool SampleFilelist::isMC() const
{ return m_isMC; }

void SampleFilelist::setWeighted(bool weighted)
{ m_weighted=weighted; }

bool SampleFilelist::weighted() const
{ return m_weighted; }

std::vector<std::string> SampleFilelist::filelist()
{
  return m_filelist;
}

std::unordered_map<uint32_t, double> SampleFilelist::scales()
{
  init_meta();

  std::unordered_map<uint32_t, double> theScales=m_norm;
  for(std::pair<uint32_t, double> kv : m_norm)
    theScales[kv.first]=scale()/kv.second;

  return theScales;
}

void SampleFilelist::init_meta()
{
  if(m_isMetaLoaded) return;

  // Normaliation
  if(isMC())
    {
      m_norm.clear();

      int32_t mcChannelNumber=0;
      int32_t runNumber      =0;

      for(const std::string& file : m_filelist)
	{
	  TFile *fh=TFile::Open(file.c_str());
	  TTree *t=fh->Get<TTree>("outTree");
	  if(t!=nullptr)
	    {
	      // Determine what is in the sample
	      t->SetBranchStatus ("*"              ,0);
	      t->SetBranchStatus ("runNumber"      ,1);
	      t->SetBranchStatus ("mcChannelNumber",1);
	      t->SetBranchAddress("runNumber"      ,&runNumber);
	      t->SetBranchAddress("mcChannelNumber",&mcChannelNumber);
	      t->GetEntry(0);

	      // Get normalization
	      uint32_t dskey=runNumber*1000+mcChannelNumber;

	      TH1D *h=fh->Get<TH1D>("MetaData_EventCount");
	      if(m_norm.count(dskey)==0)
		m_norm[dskey] =(m_weighted)?h->GetBinContent(3):h->GetBinContent(1);
	      else
		m_norm[dskey]+=(m_weighted)?h->GetBinContent(3):h->GetBinContent(1);
	    }
	  fh->Close();
	}
    }

  m_isMetaLoaded=true;
}

