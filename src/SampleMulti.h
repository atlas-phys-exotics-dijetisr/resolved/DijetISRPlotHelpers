#ifndef SAMPLEMULTI_H
#define SAMPLEMULTI_H

#include <vector>
#include <string>
#include <memory>

#include <ROOT/RDataFrame.hxx>

#include "Sample.h"

/**
 * \brief Sample consisting of muliple samples
 */
class SampleMulti : public Sample
{
public:
  SampleMulti() =default;

  void addSample(std::shared_ptr<Sample> sample);

  virtual bool isMC() const;

  virtual std::vector       <std::string     > filelist();
  virtual std::unordered_map<uint32_t, double> scales  ();

private:
  std::vector<std::shared_ptr<Sample>> m_samples;
};

#endif // SAMPLEMULTI_H
