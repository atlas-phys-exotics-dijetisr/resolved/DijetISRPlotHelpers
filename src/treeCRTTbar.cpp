#include <iostream>
#include <fstream>
#include <chrono>
#include <algorithm>
#include <getopt.h>

#include <TFile.h>
#include <TH1D.h>
#include <TLorentzVector.h>
#include <ROOT/RDataFrame.hxx>

#include "SampleFactory.h"
#include "CRTTbarAnalysis.h"

// -- Options --
uint32_t nthreads=0;
bool btagging_syst=false;
//

void usage(char* argv[])
{
  std::cerr << "Usage: " << argv[0] << " [options] outDir [sample [systematic [selection]]]" << std::endl;
  std::cerr << "List of options:" << std::endl;
  std::cerr << " -t, --threads  Number of threads (default: " << nthreads << ")" << std::endl;
  std::cerr << " -b, --btagsys  Run btagging systematics"  << std::endl;
  std::cerr << "" << std::endl;
}

int main(int argc, char* argv[])
{
  if (argc < 1)
    {
      usage(argv);
      return 1;
    }

  int c;
  while (1)
    {
      int option_index = 0;
      static struct option long_options[] =
        {
          {"threads", required_argument, 0,  't' },
          {"btagsys", no_argument      , 0,  'b' },
          {0,         0,              0, 0       }
        };

      c = getopt_long(argc, argv, "t:b", long_options, &option_index);
      if (c == -1)
        break;

      switch (c)
        {
        case 't':
          nthreads = std::stoi(optarg);
          break;
        case 'b':
          btagging_syst = true;
          break;
        default:
          std::cerr << "Invalid option supplied. Aborting." << std::endl;
          std::cerr << std::endl;
          usage(argv);
	  return 1;
        }
    }

  if (optind == argc)
    {
      std::cerr << "Required outDir argument missing." << std::endl;
      std::cerr << std::endl;
      usage(argv);
      return 1;
    }

  // Parse arguments
  if(argc<3)
    {
      std::cerr << "usage: " << argv[0] << " nthreads outDir [sample [systematic [selection]]]" << std::endl;
      return 1;
    }
  std::string outDir=argv[optind++];
  std::vector<std::string> saves={"Powheg_ttbar.mc16e", "data18"};
  std::vector<std::string> systematics={""};
  std::vector<std::string> selections={"crttbar"};

  if(optind<argc)
    saves={argv[optind++]};

  if(optind<argc)
    {
      std::string systematic=argv[optind++];
      if(systematic=="nominal")
	systematics={""};
      else
	systematics={systematic};
    }

  if(optind<argc)
    selections={argv[optind++]};
  
  ROOT::EnableImplicitMT(nthreads);  

  // Process stuff
  SampleFactory sf("../samples.yml");

  std::map<std::string, std::shared_ptr<ROOT::RDF::RNode> CRTTbarAnalysis::*> selectionMap;
  selectionMap["crttbar"] = &CRTTbarAnalysis::df_crttbar;

  for(const std::string& save : saves)
    {
      std::cout << "Processing " << save << std::endl;
      if(!sf.contains(save))
	{
	  std::cerr << "WARNING: Unable to find \"" << save << "\"! Skip..." << std::endl;
	  continue;
	}

      for(const std::string& fatjetSyst : systematics)
	{
	  std::vector<std::string> branchList =  {"Hcand_pt"             ,"Hcand_m"            ,
						  "Hcand_uncorr_pt"      ,"Hcand_uncorr_m"     ,
						  "tag_fatjet_pt"        ,"tag_fatjet_m"       ,
						  "tag_fatjet_uncorr_pt" ,"tag_fatjet_uncorr_m",
           					  "w"                    ,"runNumber"           };

	  std::string outFilePrefix=outDir+"/"+save+"_";
	  std::string outFileSuffix=".root";
	  std::string outTree="outTree";

	  // Initialize the analysis class
	  if(fatjetSyst.empty())
	    {
	      std::cout << "\tnominal" << std::endl;
	    }
	  else
	    {
	      std::cout << "\tsystematic " << fatjetSyst << std::endl;
	      outTree+=fatjetSyst;
	      outFileSuffix="_"+fatjetSyst+outFileSuffix;
	    }

	  std::shared_ptr<ROOT::RDF::RNode> df=sf[save]->dataframe();
	  uint32_t nevents=df->Count().GetValue();
	  CRTTbarAnalysis a(df,sf[save]->isMC(),fatjetSyst);

	  //
	  // Add any branches that depend on sample/systematic
	  if(sf[save]->isMC())
	    {
	      branchList.push_back("mcChannelNumber");
	    }	  

	  // btagging systematics
	  if(sf[save]->isMC() && btagging_syst)
	    {
	      for(const std::string& btagsyst : a.btagging_systematics)
		{
		  if(btagsyst.empty()) continue;
		  branchList.push_back("w"+btagsyst);
		}
	    }

	  //
	  // Loop the loop!
	  for(const std::string& selection : selections)
	    {
	      std::cout << "\t\t" << selection << std::endl;
	      if(selectionMap.find(selection)==selectionMap.end())
		{
		  std::cerr << "WARNING: Unable to find \"" << selection << "\"! Skip..." << std::endl;
		  continue;
		}
	      
	      std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
	      (a.*(selectionMap[selection]))->Snapshot(outTree,outFilePrefix+selection+outFileSuffix,branchList);
	      std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	      std::cout << "\t\t" << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " ms, " << nevents / std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " Hz" << std::endl;
	    }
	}
    }
}
