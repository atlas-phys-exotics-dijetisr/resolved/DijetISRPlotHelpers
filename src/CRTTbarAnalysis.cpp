#include "CRTTbarAnalysis.h"

#include "Defines.h"
#include "CRTTbarDefines.h"

CRTTbarAnalysis::CRTTbarAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC, const std::string& syst)
  : m_isMC(isMC)
{
  df_root = rootdf;
  
  // Filter out events without at least one muon and two fatjets w/ pt > 250.0
  // TODO: consider adding min muonPt > 40.0 (make it faster)
  df_filtered = std::make_shared<ROOT::RDF::RNode> ((*df_root)
						    .Filter([](int32_t nmuon) { return nmuon>0; }, {"nmuon"})
						    .Filter([](int32_t nfatjet) { return nfatjet>1; }, {"nfatjet"+syst})
						    .Filter([](const std::vector<float>& fatjet_pt)-> bool
							    { return fatjet_pt[0] > 250.0 && fatjet_pt[1] > 250.0; },
							    {"fatjet" + syst + "_pt"}));

  // Definitions of four-momenta
  df_defp4 = std::make_shared<ROOT::RDF::RNode> ((*df_filtered)
						 .Define("fatjet" + syst + "_p4", PtEtaPhiM, 
							 {"nfatjet" + syst, "fatjet" + syst + "_pt", "fatjet" + syst + "_eta","fatjet" + syst + "_phi", "fatjet" + syst + "_m"})
						 .Define("trkjet_p4", PtEtaPhiE,
							 {"ntrkjet", "trkjet_pt", "trkjet_eta", "trkjet_phi", "trkjet_E"})
						 .Define("muon_p4", PtEtaPhiM,
							 {"nmuon", "muon_pt", "muon_eta", "muon_phi", "muon_m"}));

  df_prw = std::make_shared<ROOT::RDF::RNode> ((*df_defp4)
					       .Define("weight_prw",[](float weight, float weight_pileup)->float
						       { return weight*weight_pileup;}, {"weight_norm", "weight_pileup"}));

  if(isMC)
    df_trigger = std::make_shared<ROOT::RDF::RNode> ((*df_prw)
						     .Filter(filter_trigger_or1, {"trigger_HLT_mu50"})
						     .Define("weight_trigger", [](int runNumber, float weight) -> float {
							 switch(runNumber)
							   { // Need to check these luminosities
							   case 284500: return 36.2e3*weight; //mc16a
							   case 300000: return 44.3e3*weight; //mc16d
							   case 310000: return 58.5e3*weight; //mc16e
							   }
							 return 1.;
						       }, {"runNumber", "weight_prw"}));
  else
    df_trigger = std::make_shared<ROOT::RDF::RNode> ((*df_prw)
						     .Filter(filter_trigger_or1, {"trigger_HLT_mu50"})
						     .Define("weight_trigger", []() -> float { return 1.;}));

  df_triggermuon = std::make_shared<ROOT::RDF::RNode> ((*df_trigger)
						       .Filter([](const std::vector<std::string>& muon_listTrigChains, const std::vector<std::vector<int32_t> >& muon_isTrigMatchedToChain)->bool {
							   for(uint32_t i=0;i<muon_listTrigChains.size();i++)
							     {
							       if(muon_listTrigChains[i]=="HLT_mu50") return muon_isTrigMatchedToChain[0][i];
							     }
							   return false;
							 }, {"muon_listTrigChains","muon_isTrigMatchedToChain"})
						       );
  
  // cleaning
  df_cleaning = std::make_shared<ROOT::RDF::RNode> ((*df_triggermuon)
						    .Filter(filter_flag, {"eventClean_LooseBad"}));
  
  // trackjet overlap
  df_trkjet_btag = std::make_shared<ROOT::RDF::RNode> ((*df_cleaning)
						       .Define("trkjet_R", trackjet_VR, {"trkjet_pt"})
						       .Define("trkjet_overlap", trkjet_overlap,
							       {"trkjet_p4", "trkjet_R"})
						       .Define("trkjet_btag", trkjet_btagged,
							       {"trkjet_is_MV2c10_FixedCutBEff_77", "trkjet_overlap" })
						       .Define("fatjet" + syst + "_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet", fatjet_trkjet10_idxs,
							       {"fatjet" + syst + "_trkJetsIdx_GhostVR30Rmax4Rmin02TrackJet","trkjet_pt"}));

  // tag muons
  df_muon_pt = std::make_shared<ROOT::RDF::RNode> ((*df_trkjet_btag)
						   .Filter([](const std::vector<float>& muon_pt)->bool { return muon_pt[0]>1.05*50; }, {"muon_pt"})
						   );

  // tag fatjets
  df_tag_fatjet = std::make_shared<ROOT::RDF::RNode> ((*df_muon_pt)
						      .Define("tag_fatjet_idx", CRTTbar::tag_fatjet_idx,
							      {"muon_p4", "fatjet" + syst + "_p4"})
						      .Filter([](uint32_t tag_fatjet_idx)->bool { return tag_fatjet_idx != 666; }, {"tag_fatjet_idx"})
						      .Define("tagjet_nbtag", CRTTbar::tagjet_nbtag,
							      {"tag_fatjet_idx", "fatjet" + syst + "_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet", "trkjet_btag"} )
						      .Filter([](uint32_t nbtag)->bool { return nbtag == 1; }, {"tagjet_nbtag"}));

  // probe fatjets
  df_probe_fatjet = std::make_shared<ROOT::RDF::RNode> ((*df_tag_fatjet)
							.Define("Hcand_idx", CRTTbar::probe_fatjet_idx, {"tag_fatjet_idx"})
							.Filter([](uint32_t probe_fatjet_idx)->bool { return probe_fatjet_idx != 666; }, {"Hcand_idx"})
							.Define("probejet_nbtag", CRTTbar::probejet_nbtag,
								{"Hcand_idx", "fatjet"+syst+"_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet", "trkjet_btag"} )
							.Filter([](uint32_t nbtag)->bool { return nbtag == 1; }, {"probejet_nbtag"}));

  // hemisphere requirement
  df_hemisphere = std::make_shared<ROOT::RDF::RNode> ((*df_probe_fatjet)
						      .Filter(CRTTbar::filter_probe_tag_deltaPhi, {"tag_fatjet_idx", "Hcand_idx", "fatjet" + syst + "_p4"}));

  // CRttbar
  if(m_isMC)
    {
      //
      // b-tagging systematics loop
      df_crttbar = std::make_shared<ROOT::RDF::RNode> ((*df_hemisphere));

      uint32_t btagging_systematics_size=(syst.empty())?btagging_systematics.size():1;

      for(uint32_t btag_syst_idx=0;
	  btag_syst_idx<btagging_systematics_size;
	  btag_syst_idx++)
	{
	  std::string bsystname=btagging_systematics[btag_syst_idx];

	  df_crttbar=std::make_shared<ROOT::RDF::RNode>((*df_crttbar)
							.Define("w"+bsystname, [btag_syst_idx](float weight, uint32_t tag_fatjet_idx, uint32_t probe_fatjet_idx, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<std::vector<float>>& trkjet_SF)->float
								{
								  return weight*CRTTbar::btagSF(tag_fatjet_idx,probe_fatjet_idx,fatjet_trkjet_idxs,trkjet_SF,btag_syst_idx);
								}, {"weight_trigger","tag_fatjet_idx", "Hcand_idx", "fatjet" + syst + "_trkJets10Idx_GhostVR30Rmax4Rmin02TrackJet" ,"trkjet_SF_MV2c10_FixedCutBEff_77"})
							);
	}
    }
  else
    {
      df_crttbar = std::make_shared<ROOT::RDF::RNode> ((*df_hemisphere)
						       .Define("w", []()->float { return 1.; })
							);
    }

  // other useful defines
  df_crttbar = std::make_shared<ROOT::RDF::RNode> ((*df_crttbar)
						   .Define("Hcand_pt", define_float_idx,
							   {"fatjet" + syst + "_muonCorrected_pt", "Hcand_idx"})
						   .Define("Hcand_uncorr_pt",
							   define_float_idx,
							   {"fatjet" + syst + "_pt", "Hcand_idx"})
						   .Define("Hcand_m",
							   define_float_idx,
							   {"fatjet" + syst + "_muonCorrected_m", "Hcand_idx"})
						   .Define("Hcand_uncorr_m",
							   define_float_idx,
							   {"fatjet" + syst + "_m", "Hcand_idx"})
						   .Define("tag_fatjet_pt",
							   define_float_idx,
							   {"fatjet" + syst + "_muonCorrected_pt", "tag_fatjet_idx"})
						   .Define("tag_fatjet_uncorr_pt",
							   define_float_idx,
							   {"fatjet" + syst + "_pt", "tag_fatjet_idx"})
						   .Define("tag_fatjet_m",
							   define_float_idx,
							   {"fatjet" + syst + "_muonCorrected_m", "tag_fatjet_idx"})
						   .Define("tag_fatjet_uncorr_m",
							   define_float_idx,
							   {"fatjet" + syst + "_m", "tag_fatjet_idx"})
						   );
}
