#ifndef SAMPLEFILELIST_H
#define SAMPLEFILELIST_H

#include <vector>
#include <string>
#include <memory>

#include <ROOT/RDataFrame.hxx>

#include "Sample.h"

/**
 * \brief Sample consisting of a list of ROOT files
 */
class SampleFilelist : public Sample
{
public:
  SampleFilelist(const std::vector<std::string>& filelist, bool isMC=false);
  SampleFilelist(bool isMC=false);

  void addFile    (const std::string& path);
  void addFilelist(const std::string& path);

  void setWeighted(bool weighted);
  bool weighted() const;

  virtual bool isMC() const;  

  virtual std::vector       <std::string     > filelist();
  virtual std::unordered_map<uint32_t, double> scales  ();

private:
  void init_meta();
  
  //!breif metadata has been loaded from the ROOT fiels
  bool m_isMetaLoaded=false;

  bool m_isMC     =false;
  bool m_weighted =false;

  std::unordered_map<uint32_t, double> m_norm;
  
  std::vector<std::string> m_filelist;
};

#endif // SAMPLEFILELIST_H
