#ifndef SAMPLEFACTORY_H
#define SAMPLEFACTORY_H

#include <string>
#include <unordered_map>
#include <memory>

#include "Sample.h"

class SampleFactory
{
public:
  SampleFactory(const std::string& samplesFile);

  bool contains(const std::string& sampleName) const;

  std::shared_ptr<Sample> operator[] (const std::string& sampleName);

private:
  std::unordered_map<std::string,std::shared_ptr<Sample>> m_samples;
};

#endif // SAMPLEFACTORY_H
