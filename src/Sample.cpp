#include "Sample.h"

#include <TFile.h>

void Sample::setScale(double scale)
{ m_scale=scale; }

double Sample::scale() const
{ return m_scale; }

void Sample::setWeightIdx(uint32_t idx)
{
  m_weightIdx=idx;
}

uint32_t Sample::weightIdx() const
{
  return m_weightIdx;
}


void Sample::init_root()
{
  // Check for empty files and MC metadata
  std::vector<std::string> theFilelist;
  for(const std::string& path : filelist())
    {
      TFile *fh=TFile::Open(path.c_str());
      if(fh==nullptr)
	throw std::runtime_error("Unable to open file " + path);

      TTree *t=fh->Get<TTree>("outTree");
      if(t!=nullptr)
	theFilelist.push_back(path);
      fh->Close();
    }

  // init
  m_root=std::make_shared<ROOT::RDataFrame>("outTree",theFilelist);
}

std::shared_ptr<ROOT::RDataFrame> Sample::root()
{
  if(!m_root)
    init_root();

  return m_root;
}

std::shared_ptr<ROOT::RDF::RNode> Sample::dataframe()
{
  if(!m_root)
    init_root();

  if(isMC())
    {
      std::unordered_map<uint32_t, double> theScales=scales();
      uint32_t thisWeightIdx=weightIdx();

      if(m_weightIdx==0)
	return std::make_shared<ROOT::RDF::RNode>((*m_root)
						  .Define("weight_norm",
							  [theScales](int runNumber, int mcChannelNumber, float weight) -> float { return weight*theScales.at(1000*runNumber+mcChannelNumber); },
							  {"runNumber","mcChannelNumber","weight"})
						  );
      else
	return std::make_shared<ROOT::RDF::RNode>((*m_root)
						  .Define("weight_norm",
							  [theScales,thisWeightIdx](int runNumber, int mcChannelNumber, float weight, const std::vector<float>& mcEventWeights) -> float { return weight*theScales.at(1000*runNumber+mcChannelNumber)*mcEventWeights[thisWeightIdx]/mcEventWeights[0]; },
							  {"runNumber","mcChannelNumber","weight","mcEventWeights"})
						  );
    }
  else
    {
      return std::make_shared<ROOT::RDF::RNode>((*m_root)
						.Define("weight_norm", []() -> float {return 1.;})
						);
    }
      
}



