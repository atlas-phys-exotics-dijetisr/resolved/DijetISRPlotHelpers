#include <iostream>
#include <fstream>
#include <chrono>
#include <algorithm>

#include <TFile.h>
#include <TH1D.h>
#include <TLorentzVector.h>
#include <ROOT/RDataFrame.hxx>

#include "SampleFactory.h"
#include "ListBasedHbbAnalysis.h"

int main()
{
  ROOT::EnableImplicitMT();

  SampleFactory sf("../samples.yml");

  ListBasedHbbAnalysis a(sf["Higgs_ggf.mc16d"]->dataframe(), sf["Higgs_ggf.mc16d"]->isMC());

  std::cout << "Preselected\t"    << a.df_root      ->Count().GetValue() << "\t" << a.df_root      ->Sum("weight_norm"   ).GetValue()*44.3e3 << std::endl;
  std::cout << "Trigger\t"        << a.df_lumi      ->Count().GetValue() << "\t" << a.df_lumi      ->Sum("weight_trigger").GetValue() << std::endl;
  std::cout << "Trigger Jet\t"    << a.df_triggerjet->Count().GetValue() << "\t" << a.df_triggerjet->Sum("weight_trigger").GetValue() << std::endl;
  std::cout << "Jet Cleaning\t"   << a.df_cleaning  ->Count().GetValue() << "\t" << a.df_cleaning  ->Sum("weight_trigger").GetValue() << std::endl;
  std::cout << "Dijet\t"          << a.df_dijet     ->Count().GetValue() << "\t" << a.df_dijet     ->Sum("weight_trigger").GetValue() << std::endl;
  std::cout << "Non-empty List\t" << a.df_fatjetlist->Count().GetValue() << "\t" << a.df_category  ->Sum("weight_trigger").GetValue() << std::endl;

  std::cout << "SRL\t" << a.df_sf_srl->Count().GetValue() << "\t" << a.df_sf_srl->Sum("w").GetValue() << std::endl;
  std::cout << "SRS\t" << a.df_sf_srs->Count().GetValue() << "\t" << a.df_sf_srs->Sum("w").GetValue() << std::endl;
  std::cout << "VRL\t" << a.df_sf_vrl->Count().GetValue() << "\t" << a.df_sf_vrl->Sum("w").GetValue() << std::endl;
  std::cout << "VRS\t" << a.df_sf_vrs->Count().GetValue() << "\t" << a.df_sf_vrs->Sum("w").GetValue() << std::endl;
}
