#ifndef LISTBASEDHBBANALYSIS_H
#define LISTBASEDHBBANALYSIS_H

#include <memory>

#include <ROOT/RDataFrame.hxx>

class ListBasedHbbAnalysis
{
public:
  ListBasedHbbAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC=false, const std::string& syst="");

  // Different nodes
  std::shared_ptr<ROOT::RDF::RNode> df_root;
  std::shared_ptr<ROOT::RDF::RNode> df_defp4;

  std::shared_ptr<ROOT::RDF::RNode> df_prw;
  std::shared_ptr<ROOT::RDF::RNode> df_trigger;
  std::shared_ptr<ROOT::RDF::RNode> df_lumi;
  std::shared_ptr<ROOT::RDF::RNode> df_triggerjet;
  std::shared_ptr<ROOT::RDF::RNode> df_cleaning;
  std::shared_ptr<ROOT::RDF::RNode> df_dijet;

  std::shared_ptr<ROOT::RDF::RNode> df_trkJet10Idx;  

  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_idx;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_ntj;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_nbin2;  
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_vrolap;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjet_boost;
  std::shared_ptr<ROOT::RDF::RNode> df_fatjetlist;

 
  std::shared_ptr<ROOT::RDF::RNode> df_category;

  std::shared_ptr<ROOT::RDF::RNode> df_sf_srl;
  std::shared_ptr<ROOT::RDF::RNode> df_sf_vrl;
  std::shared_ptr<ROOT::RDF::RNode> df_sf_srs;
  std::shared_ptr<ROOT::RDF::RNode> df_sf_vrs;
  
  std::shared_ptr<ROOT::RDF::RNode> df_srl;
  std::shared_ptr<ROOT::RDF::RNode> df_vrl;
  std::shared_ptr<ROOT::RDF::RNode> df_srs;
  std::shared_ptr<ROOT::RDF::RNode> df_vrs;
};

#endif // LISTBASEDHBBANALYSIS_H
