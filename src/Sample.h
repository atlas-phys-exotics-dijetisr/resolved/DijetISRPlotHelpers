#ifndef SAMPLE_H
#define SAMPLE_H

#include <vector>
#include <string>
#include <memory>

#include "ROOT/RDataFrame.hxx"

class Sample
{
public:
  Sample() =default;

  void setScale(double scale);
  double scale() const;

  void setWeightIdx(uint32_t idx);
  uint32_t weightIdx() const;
  
  virtual bool isMC() const =0;

  std::shared_ptr<ROOT::RDataFrame> root     ();
  std::shared_ptr<ROOT::RDF::RNode> dataframe();

  virtual std::vector       <std::string     > filelist() =0;
  virtual std::unordered_map<uint32_t, double> scales  () =0;

protected:  
  virtual void init_root();
  std::shared_ptr<ROOT::RDataFrame> m_root=nullptr;
private:
  double m_scale=1.;
  uint32_t m_weightIdx=0;
};

#endif // SAMPLE_H
