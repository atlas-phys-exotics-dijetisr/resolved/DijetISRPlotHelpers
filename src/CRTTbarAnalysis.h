#ifndef CRTTBARANALYSIS_H
#define CRTTBARANALYSIS_H

#include <memory>

#include <ROOT/RDataFrame.hxx>

class CRTTbarAnalysis
{
public:
  CRTTbarAnalysis(std::shared_ptr<ROOT::RDF::RNode> rootdf, bool isMC=false, const std::string& syst="");

  // List of b-tagging systematics considered
  std::vector<std::string> btagging_systematics={"","FT_EFF_Eigen_B_0__1down","FT_EFF_Eigen_B_0__1up","FT_EFF_Eigen_B_1__1down","FT_EFF_Eigen_B_1__1up","FT_EFF_Eigen_B_2__1down","FT_EFF_Eigen_B_2__1up","FT_EFF_Eigen_B_3__1down","FT_EFF_Eigen_B_3__1up","FT_EFF_Eigen_B_4__1down","FT_EFF_Eigen_B_4__1up","FT_EFF_Eigen_C_0__1down","FT_EFF_Eigen_C_0__1up","FT_EFF_Eigen_C_1__1down","FT_EFF_Eigen_C_1__1up","FT_EFF_Eigen_C_2__1down","FT_EFF_Eigen_C_2__1up","FT_EFF_Eigen_C_3__1down","FT_EFF_Eigen_C_3__1up","FT_EFF_Eigen_C_4__1down","FT_EFF_Eigen_C_4__1up","FT_EFF_Eigen_Light_0__1down","FT_EFF_Eigen_Light_0__1up","FT_EFF_Eigen_Light_1__1down","FT_EFF_Eigen_Light_1__1up","FT_EFF_Eigen_Light_2__1down","FT_EFF_Eigen_Light_2__1up","FT_EFF_Eigen_Light_3__1down","FT_EFF_Eigen_Light_3__1up","FT_EFF_Eigen_Light_4__1down","FT_EFF_Eigen_Light_4__1up","FT_EFF_Eigen_Light_5__1down","FT_EFF_Eigen_Light_5__1up","FT_EFF_extrapolation__1down","FT_EFF_extrapolation__1up","FT_EFF_extrapolation_from_charm__1down","FT_EFF_extrapolation_from_charm__1up"};
  
  // Different nodes
  std::shared_ptr<ROOT::RDF::RNode> df_root;
  // filtered for events w/ at least a muon and two fatjets w/ pt > 250
  std::shared_ptr<ROOT::RDF::RNode> df_filtered;
  // four-momenta
  std::shared_ptr<ROOT::RDF::RNode> df_defp4;
  // pile-up reweight
  std::shared_ptr<ROOT::RDF::RNode> df_prw;
  // trigger weight
  std::shared_ptr<ROOT::RDF::RNode> df_trigger;
  // trigger muon match
  std::shared_ptr<ROOT::RDF::RNode> df_triggermuon;
  // jet cleaning
  std::shared_ptr<ROOT::RDF::RNode> df_cleaning;
  // btag for non-overlapping trkjets
  std::shared_ptr<ROOT::RDF::RNode> df_trkjet_btag;
  // high pT muon
  std::shared_ptr<ROOT::RDF::RNode> df_muon_pt;
  // Tag fatjet
  std::shared_ptr<ROOT::RDF::RNode> df_tag_fatjet;
  // Tag probe_jet
  std::shared_ptr<ROOT::RDF::RNode> df_probe_fatjet;
  // dPhi checks
  std::shared_ptr<ROOT::RDF::RNode> df_hemisphere;
  // Product of scale factors
  std::shared_ptr<ROOT::RDF::RNode> df_scale_factors;
  // CRttbar
  std::shared_ptr<ROOT::RDF::RNode> df_crttbar;


private:
  bool m_isMC = false;
};

#endif // CRTTBARANALYSIS_H
