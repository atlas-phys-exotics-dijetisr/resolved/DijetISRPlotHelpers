#ifndef DEFINES_H
#define DEFINES_H

#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>

#define EF_prescaled (0x1 << 2)

ROOT::VecOps::RVec<TLorentzVector> PtEtaPhiM(int32_t n, const std::vector<float>& pt, const std::vector<float>& eta, const std::vector<float>& phi, const std::vector<float>& m);
ROOT::VecOps::RVec<TLorentzVector> PtEtaPhiE(int32_t n, const std::vector<float>& pt, const std::vector<float>& eta, const std::vector<float>& phi, const std::vector<float>& e);

bool filter_flag(bool flag);

bool filter_or(bool column0, bool column1, bool column2, bool column3);

bool filter_trigger_or1(int8_t trigger0);
bool filter_trigger_or4(int8_t trigger0, int8_t trigger1, int8_t trigger2, int8_t trigger3);

bool trigger_jet_ptmass(const std::vector<float>& fatjet_pt, const std::vector<float>& fatjet_m, float ptcut, float mcut);

float    define_float_idx  (const std::vector       <float   >& vec, uint32_t idx);
float    define_float_ridx (const ROOT::VecOps::RVec<float   >& vec, uint32_t idx);
uint32_t define_uint32_ridx(const ROOT::VecOps::RVec<uint32_t>& vec, uint32_t idx);

//!DeltaR between two objects
/**
 * Wraparound of phi correctly applied
 *
 * \param eta0 eta of particle 0
 * \param phi0 phi of particle 0
 * \param eta1 eta of particle 1
 * \param phi1 phi of particle 1
 *
 * \return sqrt((eta0-eta1)^2+(phi0-phi1)^2)
 */
float DeltaR(float eta0, float phi0, float eta1, float phi1);

//!DeltaPhi between two objects
/**
 * Between [0,pi]
 *
 * \param phi0 phi of particle 0
 * \param phi1 phi of particle 1
 *
 * \return |phi0-phi1|
 */
float DeltaPhi(float phi0, float phi1);

ROOT::VecOps::RVec<uint32_t> indexlist(int32_t n);

ROOT::VecOps::RVec<float> fatjet_boost(const std::vector<float>& trkjet_pt,const std::vector<float>& trkjet_m);

ROOT::VecOps::RVec<float> trackjet_VR(const std::vector<float>& trkjet_pt);
ROOT::VecOps::RVec<bool> fatjet_VR_overlap(const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_p4, const ROOT::VecOps::RVec<float>& trkjet_R);
ROOT::VecOps::RVec<bool> fatjet_VR_overlap2(const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_p4, const ROOT::VecOps::RVec<float>& trkjet_R);
ROOT::VecOps::RVec<bool> fatjet_VR_overlap3(const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_p4, const ROOT::VecOps::RVec<float>& trkjet_R);
ROOT::VecOps::RVec<bool> fatjet_VR_overlapN(uint32_t N, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const ROOT::VecOps::RVec<TLorentzVector>& trkjet_p4, const ROOT::VecOps::RVec<float>& trkjet_R);

std::vector<std::vector<uint32_t>> fatjet_trkjet5_idxs (const std::vector<std::vector<uint32_t>>& idxs, const std::vector<float>& trkjet_pt);
std::vector<std::vector<uint32_t>> fatjet_trkjet10_idxs(const std::vector<std::vector<uint32_t>>& idxs, const std::vector<float>& trkjet_pt);

//! Count number of subjets
/**
 * \param idxs fat jet to track jet association
 *
 * \return list with elements being the number of track jets in the fat jet
 */
ROOT::VecOps::RVec<uint32_t> count_subjet   (const std::vector<std::vector<uint32_t>>& idxs);

ROOT::VecOps::RVec<uint32_t> count_bjetsin2 (const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int>& trkjet_btag);
ROOT::VecOps::RVec<uint32_t> count_bjetsin3 (const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int>& trkjet_btag);
ROOT::VecOps::RVec<uint32_t> count_bjetsinN (uint32_t N, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int>& trkjet_btag);
ROOT::VecOps::RVec<uint32_t> count_truthjets(int32_t truthid, const std::vector<std::vector<uint32_t>>& fatjet_trkjet_idxs, const std::vector<int32_t>& trkjet_truth);

// T/F if the trkjet has overlap
ROOT::VecOps::RVec<bool> trkjet_overlap(const ROOT::VecOps::RVec<TLorentzVector>& trkjet_p4, const ROOT::VecOps::RVec<float>& trkjet_R);

// T/F if the non-overlapped trkjet is btagged
ROOT::VecOps::RVec<bool> trkjet_btagged(const std::vector<int>& trkjet_btag, const ROOT::VecOps::RVec<bool>& trkjet_overlap);
#endif // DEFINES_H
