#include <iostream>
#include <fstream>
#include <chrono>
#include <algorithm>

#include <TFile.h>
#include <TH1D.h>
#include <TLorentzVector.h>
#include <ROOT/RDataFrame.hxx>

#include "SampleFactory.h"
#include "CONFHbbAnalysis.h"


int main(int argc, char* argv[])
{
  // Parse arguments
  if(argc!=3)
    {
      std::cerr << "usage: " << argv[0] << " nthreads sample" << std::endl;
      return 1;
    }
  uint32_t nthreads=std::atoi(argv[1]);
  std::string sampleName=argv[2];

  ROOT::EnableImplicitMT(nthreads);  

  // Process stuff
  SampleFactory sf("../time_samples.yml");

  std::shared_ptr<Sample> sample=sf[sampleName];

  std::shared_ptr<ROOT::RDF::RNode> df=sample->dataframe();
  uint32_t nevents=df->Count().GetValue();
  CONFHbbAnalysis a(df,sample->isMC());

  std::cerr << "start running" << std::endl;
  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
  TH1D h=a.df_sr->Histo1D({"Hcand_m","",40,0,200},"Hcand_m").GetValue();
  std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
  std::cout << nthreads << "\t" << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << "\t" << nevents << std::endl;
}
