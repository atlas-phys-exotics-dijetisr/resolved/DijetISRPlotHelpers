#include "SampleMulti.h"

#include <TFile.h>

void SampleMulti::addSample(std::shared_ptr<Sample> sample)
{
  m_samples.push_back(sample);
}

bool SampleMulti::isMC() const
{ return (m_samples.size()>0)?m_samples[0]->isMC():false; }

std::vector<std::string> SampleMulti::filelist()
{
  std::vector<std::string> theFilelist;

  for(std::shared_ptr<Sample> sample : m_samples)
    {
      std::vector<std::string> sFilelist=sample->filelist();
      theFilelist.insert(theFilelist.begin(), sFilelist.begin(), sFilelist.end());
    }
  
  return theFilelist;
}

std::unordered_map<uint32_t, double> SampleMulti::scales()
{
  std::unordered_map<uint32_t, double> theScales;

  for(std::shared_ptr<Sample> sample : m_samples)
    {
      std::unordered_map<uint32_t, double> sScales=sample->scales();
      theScales.insert(sScales.begin(), sScales.end());
    }

  for(std::pair<uint32_t, double> kv : theScales)
    theScales[kv.first]*=scale();

  return theScales;
}
