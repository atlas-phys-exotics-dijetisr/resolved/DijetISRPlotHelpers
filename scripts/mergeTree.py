#!/usr/bin/env python

import glob
import os.path
import ROOT

selections={'srl','vrl','srs','vrs'}

def extract_parts(filename):
    parts=os.path.basename(filename).replace('.root','').split('_')
    myselection=set(parts).intersection(selections).pop()
    selidx=parts.index(myselection)
    mysample='_'.join(parts[:selidx])
    mysyst='_'.join(parts[selidx+1:])
    return (filename,myselection,mysample,mysyst)

fh_outs={}

inputs=sorted(glob.glob('split/*root'))

# Sort by samples
samples={}
for x in inputs:
    filename,myselection,mysample,mysyst=extract_parts(x)
    if mysample not in samples:
        samples[mysample]={}
    if myselection not in samples[mysample]:
        samples[mysample][myselection]=[]
    samples[mysample][myselection].append((filename,mysyst))
    
for sample,sdata in samples.items():
    # figure out names
    print(sample)

    fh_out=ROOT.TFile.Open(sample+'.root','RECREATE')

    for selection,seldata in sdata.items():
        print(' {}'.format(selection))
        # Create output directory
        fh_out.mkdir(selection)

        for filename,syst in seldata:
            print('  {}'.format(syst if syst!='' else 'nominal'))
            # Copy the tree
            fh_in=ROOT.TFile.Open(filename)
            fh_out.Get(selection).cd()

            tree=fh_in.Get('outTree{}'.format(syst)).CloneTree()
            tree.Write()
            fh_in.Close()
    fh_out.Close()
