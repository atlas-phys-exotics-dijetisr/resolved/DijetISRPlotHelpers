#!/bin/bash

if [ ${#} != 1 ]; then
    echo "usage: ${0} tag"
    exit 1
fi

TAG=${1}

if [ ! -e ${TAG} ]; then
    mkdir ${TAG}
fi
cd ${TAG}

rucio download "user.kkrizka.*ggF*${TAG}_tree.root"

for i in $(find $(pwd) -name "user.kkrizka.mc*${TAG}*" -type d)
do
    camp=$(basename ${i} | cut -d. -f3)
    for s in $(ls ${i} | cut -d. -f3-4 | sort | uniq)
    do
	echo "SAMPLE ${camp}.${s}"
	flname=${camp}.${s}.txt
	if [[ ${i} == /eos* ]]; then
	    find ${i} -name "*${s}*" -type f | sed -e 's/^/root:\/\/eosatlas.cern.ch\//' > ${flname}
	else
	    find ${i} -name "*${s}*" -type f > ${flname}
	fi
    done
done

for i in $(find $(pwd) -name "user.kkrizka.data*${TAG}*" -type d)
do
    year=$(basename ${i} | cut -d. -f3)
    echo "SAMPLE ${year}"
    flname=${year}.txt
    if [[ ${i} == /eos* ]]; then
	find ${i} -name "*${s}*" -type f | sed -e 's/^/root:\/\/eosatlas.cern.ch\//' > ${flname}
    else
	find ${i} -name "*${s}*" -type f > ${flname}
    fi
done
