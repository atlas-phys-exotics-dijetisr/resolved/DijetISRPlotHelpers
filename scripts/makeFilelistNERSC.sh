#!/bin/bash
if [ ${#} != 2 ]; then
    echo "usage: ${0} selection tag"
    exit -1
fi

selection=${1}
tag=${2}

# Get list of samples
for dsname in $(rucio list-dids --short user.kkrizka.*.${selection}.${tag}_tree.root)
do
    echo ${dsname}
    camp=$(echo ${dsname} | cut -d. -f4)

    for s in $(rucio list-files --csv ${dsname} | cut -d. -f 4-5 | sort | uniq)
    do
        echo "SAMPLE ${camp}.${s}"
	flname=${camp}.${s}.txt	
	rucio list-files --csv ${dsname} | grep ${s} | cut -d, -f1 | sed -e 's/^/root:\/\/dtn04.nersc.gov\/\/atlas\/rucio\//' > ${flname}

    done
done
