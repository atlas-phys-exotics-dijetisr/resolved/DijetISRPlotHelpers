#!/bin/bash

samples=(Higgs_ggf Higgs_VBF Higgs_VH Higgs_ttH data Pythia8_dijet Higgs Pythia8_ttbar Sherpa_Zqq Sherpa_Wqq)

OUTDIR=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/dibjetISR_boosted/data_20200114/cutflow

for sample in ${samples[@]}
do
    echo ./src/cutflowEventWide ${OUTDIR} ${sample}
done
